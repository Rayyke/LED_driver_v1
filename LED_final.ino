/**********************************/
/*      Author:  Peter Cizmar     */
/**********************************/

/* Note:
 *  
 * Program is set to handle around 150 LEDs. For more LEDs, you will need more SRAM(+3 bytes per one LED) and also lower maximum FPS. 
 * Setting 139 LEDs takes around 2,6ms and in 200 FPS speed, there is ~2,3ms time for serial communication (100 micro seconds
 * is safety down-time). Serial communication is important to accept the commands. If you got let´s say 450 LEDs, 
 * that´s 7,8ms setting time, and if we want to keep around ~2,1ms time for serial communication, 
 * that´s together 10ms and 1000 / 10 = 100 FPS, where 1000 represets milliseconds in 1 second. 
 * For smooth animation 30 FPS is way enaugh, but FPS is key parameter to smooth animation speeding.
 * Increasing "animationSpeed" variable will cause more choppy animations, so increase FPS if possible.
 * All these data were gathered on standard arduino UNO R3 clone (5V, 16 MHz) together with FastLED library and LED type WS2812B
 * with single wire communication.
 */

#include <FastLED.h> // controls LEDs
#include <limits.h>  // helps to handle overflowing
#include <EEPROM.h>  // handles EEPROM read/write

/* Saves 720 bytes of SRAM if 1.
 * Color palettes will only provide 16 definition points of palette, not full 256.
 * That means, if i create color palette with some color on point 1 and then on point 9,
 * it´s wont be visible, because palette can store changes only each 16 points.
 * If I define some color at point 1 and then on point 16 (index 0 and 15), it will be fine even with this
 * saving mode on.
 * For me, not enaugh SRAM caused non-working LED strip. I had 2048 bytes of memory total (Arduino UNO R3 with ATmega328p) 
 * and 87% used, which means local variables used pretty much over 200 bytes. It was before optimalization tho. Now it´s using 
 * only 77% and local variables were also optimalized. If the LED strip isn´t wroking, at first simply try to set next value 
 * to 1. Some kind of first aid.
 */
#define SRAM_LOW_COST 0

/* Can be any avilable pin (no need for PWM or external interrupts).
 */
#define LED_DATA_PIN 3 

/* I attached small microphone module to one of the analog pins. Can be used for designing modes too.
 */
#define MIC_ANALOG_PIN A0

/* Custom high-speed approach to pin 4, which provides 0 or 1 if device is ready to receive data via serial comm.
 * or not (1 - ready, 0 - not ready, dont send anything).
 * It´s because my LED strip WS2812B has only data line, and CPU must stop even interrupts to properly send data to LEDs.
 * This was solved, CPU will inform transmitter via pin 4, if it´s ready to receive data or not.
 */
#define READY_TO_RECEIVE_PIN_INIT {DDRD |= B00010000;} // INIT of pin
#define READY_TO_RECEIVE_PIN_HIGH {PORTD |= B00010000;} // HIGH when ready
#define READY_TO_RECEIVE_PIN_LOW {PORTD &= B11101111;}  // LOW when NOT ready

/* FPS represents, how many changes will be performed on LED strip per second (from word Frames per second).
 */
#define MIN_FPS 1 // total min 1
#define MAX_FPS 200 // total max 200

/* Palette sampling represents distance between picked colors from palette, if set i.e. to 2, 
 * every 2nd color is picked from palette (palette, which has always 256 points, will fit into 128 LEDs)
 */
#define MIN_PALETTE_SAMPLING 1 // total min 1
#define MAX_PALETTE_SAMPLING 16 // total max 99

/* Animation is handled by moving the starting index across the color palette.
 * Animation speed defines, how much points on this palette should it move.  
 * The higher the number is, the faster i move across the palette, however i am
 * loosing the smoothness with the increasing number. If possible, let it be always 1.
 */
#define MIN_ANIMATION_SPEED 1 // total min 1
#define MAX_ANIMATION_SPEED 16 // total max 99

#define BLENDING_TYPE LINEARBLEND
#define BAUD_RATE 115200 // 115200 or higher recommended
#define BAUD_ONE_SYMBOL 10 // default: 1 start bit, 1 stop bit, 8 bits for symbol, total 10 bits
#define MAX_CMD_LENGTH 50 // 50 recommended (sufficient enaugh)

/* Table:  45  LEDS
 * Bed:    58  LEDS
 * Shelf:  36  LEDS
 * TOTAL:  139 LEDS
 *
 * This allows me to have 3 separate groups. I can simply execute command "DST,xyz", where x, y and z is bool number.
 * If 0, the related group will be skipped, while filling from palette and will be filled with black color. 
 * Next i will define index of beginning and ending LED of each group.
 */
#define TABLE_BEGIN_LED 0
#define TABLE_END_LED 44
#define BED_BEGIN_LED 45
#define BED_END_LED 102
#define SHELF_BEGIN_LED 103
#define SHELF_END_LED 138

#define TOTAL_LEDS 139

/*** EEPROM ADDRESS DESCRIPTION ***/
/* Additional needed valus, i.e. colors for mode 0 and 1 are stored in memory 256+ 
 */
#define EEPROM_MODE 0
#define EEPROM_BRIGHTNESS 1
#define EEPROM_FPS 2
#define EEPROM_D1 3
#define EEPROM_D2 4
#define EEPROM_D3 5
#define EEPROM_ANIMATION_SPEED 6
#define EEPROM_PALETTE_SAMPLING 7
#define EEPROM_PALETTE_ANIMATION_DIRECTION 8

/*** CUSTOM PALETTES ***/
/* I am designing my own palettes for full-range palette, SRAM_LOW_COST is disabled (set to 0)
 * All designed palettes are stored in PROGMEM and loaded to variable "palette" only when needed.
 */
DEFINE_GRADIENT_PALETTE(Heatmap_cp){
0,    0,    0,    0,
128,  255,  0,    0,
224,  255,  255,  0,
255,  255,  255,  255
};

/*** CUSTOM CLASSES ***/
class SERIAL_READER{
  public:
    char string[MAX_CMD_LENGTH + 1]; // +1 is for null
    char _char; 

    SERIAL_READER(){
      for(int i = 0; i < MAX_CMD_LENGTH; i++){
        string[i] = '\0'; 
      }

      _char = '\0';
    }

    ~SERIAL_READER(){ 
    }
};

/*** FUNCTION PROTOTYPES ***/
void(* resetFunc) (void) = 0; // resets board after RST command
void colorAutoChange(uint8_t counter);

/*** GLOBAL OBJECTS ***/
SERIAL_READER serialReader; // stores data from Serial reading
CRGB leds[TOTAL_LEDS];      // stores color status of each LED

// uses only 16-point range palette instead of 256-point palette, check more in FastLED documentation
#if SRAM_LOW_COST == 1
CRGBPalette16 palette;
#else
CRGBPalette256 palette;
#endif

/*** GLOBAL VARIABLES ***/
unsigned long stamp_t; // handles timing of FPS

/*** PARAMETERS ***/
uint8_t mode; // current mode playing (0 - 255, not all available/programmed)
uint8_t brightness; // current shown brightness (0 - 255)
uint8_t FPS; // described next to macro definitions for min and max FPS
uint8_t d1; // distribution of LEDs, table
uint8_t d2; // distribution of LEDs, bed
uint8_t d3; // distribution of LEDs, shelf
uint8_t animationSpeed; // described next to macro definitions for min and max animation speed
uint8_t paletteSampling; // described next to macro definitions for palette sampling
uint8_t paletteAnimationDirection; // direction depends on how you placed your LED strip (0 - one direction, 1 - other direction) 

void setup(void) {
  // got external LED on this pin, represets if the device is ON or OFF
  pinMode(13, OUTPUT);
  digitalWrite(13, HIGH);

  // init pin 4 as output and set to low
  READY_TO_RECEIVE_PIN_INIT
  READY_TO_RECEIVE_PIN_LOW

  // init of variable for storing micros()
  stamp_t = micros();

  // init variables from EEPROM (stored from previous run-time)
  mode = EEPROM.read(EEPROM_MODE);
  brightness = EEPROM.read(EEPROM_BRIGHTNESS);
  FPS = EEPROM.read(EEPROM_FPS);
  d1 = EEPROM.read(EEPROM_D1);
  d2 = EEPROM.read(EEPROM_D2);
  d3 = EEPROM.read(EEPROM_D3);
  animationSpeed = EEPROM.read(EEPROM_ANIMATION_SPEED);  // used for all animated modes
  paletteSampling = EEPROM.read(EEPROM_PALETTE_SAMPLING);  // used for all palette-based modes
  paletteAnimationDirection = EEPROM.read(EEPROM_PALETTE_ANIMATION_DIRECTION);  // 1 +; 0 -; only single-palette-based modes

  // init of LED strip using FastLED library
  FastLED.addLeds<WS2811, LED_DATA_PIN, GRB>(leds, TOTAL_LEDS).setCorrection( TypicalLEDStrip );
  FastLED.setDither(1);
  FastLED.setBrightness(brightness);

  // starts serial communication
  Serial.begin(BAUD_RATE);

  // init sequence, only viasual thingy, it´s like normal delay after Serial.begin(), but more fency
  initVisualLED();

  // setting properly last played mode via executeCommand, using buffer for serial comm. to save local memory
  reloadMode();
}

void loop(void) {
  // check incomming serial communication each loop
  checkSerial();

  // setting FPS cap, this will help with regulating animation speed and create time space for receiving on Serial   
  if(((micros() + ((micros() < stamp_t) ? ULONG_MAX : 0)) - stamp_t) > (1000000 / FPS)){ 
    refreshLED();
  }
  // certain time (calculated from baud rate) before changing frame will begin, tell transmitter that device can´t receive any data
  else{
    // setting early LOW by time-length of sending 1 byte at certain baud rate, so there will be enaugh time to send last byte
    if(((micros() + ((micros() < stamp_t) ? ULONG_MAX : 0)) - stamp_t) > (1000000 / FPS) - ceil(1000000.0 / ((float)BAUD_RATE / (float)BAUD_ONE_SYMBOL))){
      READY_TO_RECEIVE_PIN_LOW 
    }
  }
}

/* Function takes last played mode and properly executes it in a form of command.
 */
void reloadMode(){
  char buff_n[4];

  // clears buffer
  for(uint8_t i = 0; i < 4; i++){
    buff_n[i] = '\0';
  }
  
  if(mode < 100){
    strcat(serialReader.string, "0");

    if(mode < 10){
      strcat(serialReader.string, "0");
    }
  }

  itoa(mode, buff_n, 10); 
  strcat(serialReader.string,buff_n);
  
  if(mode != 0 && mode != 1){
    serialReader.string[3] = '\0'; 
  }
  else{
    strcat(serialReader.string,",");

    for(uint8_t i = 0; i <= 8; i++){
      if(EEPROM.read(256 + i) < 100){
        strcat(serialReader.string, "0");
    
        if(EEPROM.read(256 + i) < 10){
          strcat(serialReader.string, "0");
        }
      }

      itoa(EEPROM.read(256 + i), buff_n, 10); 
      strcat(serialReader.string,buff_n);

      if(i != 8){
        strcat(serialReader.string,",");  
      }
    }
  }  
  
  executeCommand(serialReader.string);

  // clear the buffer for serial comm. afterwards
  for(uint8_t i = 0; i < MAX_CMD_LENGTH; i++){
    serialReader.string[i] = '\0'; 
  }
}

/* Function provides some kind of visual loading, just some fency delay.
 * Helps to stable Serial.begin()
 */
void initVisualLED(){
  for(uint8_t i = 0; i < TOTAL_LEDS; i++){
    leds[i] = CRGB(0, 0, 255);
    FastLED.show();
    delay((int)(2000.0 / (float)TOTAL_LEDS)); // ~2s
  }

  for(int j = 255; j >= 0; j--){
    for(uint8_t i = 0; i < TOTAL_LEDS; i++){
      leds[i] = CRGB(0, 0, j);
    } 

    FastLED.show(); // function sends data to LEDs, based on data in global object "leds"
  }
}

/* Function refreshes LED values, based on mode and manz other parameters.
 */
void refreshLED(){
  // static variables are not lost after loop ends
  static uint8_t prevMode = mode;
  static uint8_t brightness_t = 0;                      // temporary brightness, will go only up to master brightness
  static uint8_t brightness_t_ASC = 1;                  // increasing or decreasing temporary brightness (1 +; 0 -)
  static uint8_t counter = 0;                           // helps with auto-switching colors, transition type (0 = R->G; 1 = G->B; 2 = B->R) and with filling from palette
  
  READY_TO_RECEIVE_PIN_LOW // tell the transmitter, that device cannot receive any data now

  stamp_t = micros(); // create time-stamp

  // if the mode was changed, clear some variables and reset brightness to master value
  if(prevMode != mode){
    prevMode = mode;
    
    brightness_t = 0;
    brightness_t_ASC = 1;
    counter = 0;
    FastLED.setBrightness(brightness);
  }  

  // modes are described in separate text file
  if(mode == 0){
    // do nothing, static colors
  }
  else if(mode == 1){
    FastLED.setBrightness(brightness_t);
    
    if(brightness_t_ASC == 1){
      if(brightness_t + animationSpeed > 255){
        brightness_t = 255;
        brightness_t_ASC = 0;   
      }
      else{
        brightness_t += animationSpeed;  
      }
    }

    if(brightness_t_ASC == 0){
      if(brightness_t - animationSpeed < 0){
        brightness_t = 0;
        brightness_t_ASC = 1;   
      }
      else{
        brightness_t -= animationSpeed;  
      }
    }
  }
  else if(mode == 2){
    FastLED.setBrightness(brightness_t);

    if(brightness_t_ASC == 1){
      if(brightness_t + animationSpeed > 255){
        brightness_t = 255;
        brightness_t_ASC = 0;   
      }
      else{
        brightness_t += animationSpeed;  
      }
    }

    if(brightness_t_ASC == 0){
      if(brightness_t - animationSpeed < 0){
        brightness_t = 0;
        brightness_t_ASC = 1;

        colorAutoChange(counter);
  
        if(counter < 5){
          counter++;  
        }
        else{
          counter = 0;
        }
      }
      else{
        brightness_t -= animationSpeed;  
      }
    }
  }
  else if(mode == 3){
    if(counter == 0){
      if(leds[0].r - animationSpeed < 0){
        setStaticColors(CRGB(0, 255, 0), CRGB(0, 255, 0), CRGB(0, 255, 0));
        counter = 1;
      }
      else{
        setStaticColors(CRGB(leds[0].r - animationSpeed, 
                             leds[0].g + animationSpeed, 
                             leds[0].b), 
                        CRGB(leds[0].r - animationSpeed, 
                             leds[0].g + animationSpeed, 
                             leds[0].b), 
                        CRGB(leds[0].r - animationSpeed, 
                             leds[0].g + animationSpeed, 
                             leds[0].b));
      }
    }
    else if(counter == 1){
      if(leds[0].g - animationSpeed < 0){
        setStaticColors(CRGB(0, 0, 255), CRGB(0, 0, 255), CRGB(0, 0, 255));
        counter = 2;
      }
      else{
        setStaticColors(CRGB(leds[0].r, 
                             leds[0].g - animationSpeed, 
                             leds[0].b + animationSpeed), 
                        CRGB(leds[0].r, 
                             leds[0].g - animationSpeed, 
                             leds[0].b + animationSpeed), 
                        CRGB(leds[0].r, 
                             leds[0].g - animationSpeed, 
                             leds[0].b + animationSpeed));
      }
    }
    else{ // counter == 2
      if(leds[0].b - animationSpeed < 0){
        setStaticColors(CRGB(255, 0, 0), CRGB(255, 0, 0), CRGB(255, 0, 0));
        counter = 0;
      }
      else{
        setStaticColors(CRGB(leds[0].r + animationSpeed, 
                             leds[0].g, 
                             leds[0].b - animationSpeed), 
                        CRGB(leds[0].r + animationSpeed, 
                             leds[0].g, 
                             leds[0].b - animationSpeed), 
                        CRGB(leds[0].r + animationSpeed, 
                             leds[0].g, 
                             leds[0].b - animationSpeed));
      }
    }
  }
  else if(mode >= 10 && mode <= 99){ // mode 10 - 99 is for single-palette-based modes
    fillFromPalette(counter); 

    if(paletteAnimationDirection == 1){
      counter += animationSpeed; // after 255 goes back to 0, because of overflow (uint8_t), same goes for decreasing  
    }
    else{
      counter -= animationSpeed; 
    }
  }
  else if(mode >= 100 && mode <= 199){ // mode 100 - 199 is for multi-palette-based modes
    // TODO
  }
  else{ // mode 200 - 255 is for special modes such as sound reactive modes
    // TODO  
  }

  FastLED.show(); // show changes, send data to LEDs 
  
  READY_TO_RECEIVE_PIN_HIGH // tell transmitter, that device is ready to receive data again
}

/* Function executes given command and send via serial the success or error code.
 * For more info check included text file.
 */
void executeCommand(char input[]){
  char subBuff[4];
  memcpy(subBuff, input, 3);
  subBuff[3] = '\0';

  if(strcmp(subBuff, "CFG") == 0){
    Serial.print(F("Mode: ")); // putting string to F() moves it to progmem, instead of keeping it in SRAM
    Serial.println(mode);

    Serial.print(F("Brightness: "));
    Serial.println(brightness);

    Serial.print(F("FPS: "));
    Serial.println(FPS);

    Serial.print(F("Distribution: "));
    Serial.print(d1);
    Serial.print(d2);
    Serial.println(d3);

    Serial.print(F("Animation speed: "));
    Serial.println(animationSpeed);

    Serial.print(F("Palette sampling: "));
    Serial.println(paletteSampling);

    Serial.print(F("Palette animation direction: "));
    Serial.println(paletteAnimationDirection);
  }
  else if(strcmp(subBuff, "LED") == 0){
    for (uint8_t i = 0; i < TOTAL_LEDS; i++){
      Serial.print(F("LED "));
      Serial.print(i);
      Serial.print(F(":    \t["));
      Serial.print(leds[i].r);
      Serial.print(F(", "));
      Serial.print(leds[i].g);
      Serial.print(F(", ")); 
      Serial.print(leds[i].b);
      Serial.println(F("]"));   
    }
  }
  else if(strcmp(subBuff, "RST") == 0){
    resetFunc();
  }
  else if(strcmp(subBuff, "BHT") == 0){
    memcpy(subBuff, &input[4], 3);
    subBuff[3] = '\0';
    
    setBrightness(atoi(subBuff));
  } 
  else if(strcmp(subBuff, "FPS") == 0){
    memcpy(subBuff, &input[4], 3);
    subBuff[3] = '\0';
    
    setFPS(atoi(subBuff));
  } 
  else if(strcmp(subBuff, "DST") == 0){
    setDistribution(((int)input[4]) - 48, ((int)input[5]) - 48, ((int)input[6]) - 48);
  }
  else if(strcmp(subBuff, "PSM") == 0){
    memcpy(subBuff, &input[4], 2);
    subBuff[2] = '\0';
    
    setPaletteSampling(atoi(subBuff));
  }
  else if(strcmp(subBuff, "ASP") == 0){
    memcpy(subBuff, &input[4], 2);
    subBuff[2] = '\0';
    
    setAnimationSpeed(atoi(subBuff));
  }
  else if(strcmp(subBuff, "PAD") == 0){
    setPaletteAnimationDirection(((int)input[4]) - 48);
  }
  else if(strcmp(subBuff, "000") == 0){
    mode = 0;
    EEPROM.update(EEPROM_MODE, 0);
    uint8_t arr_t[9];

    for(uint8_t i = 0; i < 9; i++){
      memcpy(subBuff, &input[((i + 1) * 4)], 3);
      subBuff[3] = '\0';
      arr_t[i] = atoi(subBuff); 
    }
    
    setStaticColors(CRGB(arr_t[0], arr_t[1], arr_t[2]),CRGB(arr_t[3], arr_t[4], arr_t[5]),CRGB(arr_t[6], arr_t[7], arr_t[8]));
    
    for(uint8_t i = 0; i < 9; i++){
      EEPROM.update(256 + i, arr_t[i]);
    }
    
    FastLED.setBrightness(brightness);
    FastLED.show();
    Serial.print(F("1,\r\n"));
  }
  else if(strcmp(subBuff, "001") == 0){
    mode = 1;
    EEPROM.update(EEPROM_MODE, 1);
    uint8_t arr_t[9];

    for(uint8_t i = 0; i < 9; i++){
      memcpy(subBuff, &input[((i + 1) * 4)], 3);
      subBuff[3] = '\0';
      arr_t[i] = atoi(subBuff); 
    }
    
    setStaticColors(CRGB(arr_t[0], arr_t[1], arr_t[2]),CRGB(arr_t[3], arr_t[4], arr_t[5]),CRGB(arr_t[6], arr_t[7], arr_t[8]));

    for(uint8_t i = 0; i < 9; i++){
      EEPROM.update(256 + i, arr_t[i]);
    }
    
    FastLED.setBrightness(0);
    FastLED.show(); 
    Serial.print(F("1,\r\n"));
  }
  else if(strcmp(subBuff, "002") == 0){
    mode = 2; 
    EEPROM.update(EEPROM_MODE, 2);

    // starting at red color (full red was kinda bright while pulsing, it was too distractive)
    setStaticColors(CRGB(200, 0, 0),
                    CRGB(200, 0, 0),
                    CRGB(200, 0, 0));

    FastLED.setBrightness(0);
    FastLED.show(); 
    Serial.print(F("1,\r\n"));
  }
  else if(strcmp(subBuff, "003") == 0){
    mode = 3; 
    EEPROM.update(EEPROM_MODE, 3);

    setStaticColors(CRGB(255, 0, 0),
                    CRGB(255, 0, 0),
                    CRGB(255, 0, 0));

    FastLED.setBrightness(brightness);
    FastLED.show(); 
    Serial.print(F("1,\r\n"));
  }
  else if(strcmp(subBuff, "010") == 0){
    mode = 10;
    EEPROM.update(EEPROM_MODE, 10);
    
    palette = Rainbow_gp;
    Serial.print(F("1,\r\n"));
  }
  else if(strcmp(subBuff, "255") == 0){
    mode = 255;
    EEPROM.update(EEPROM_MODE, 255);
 
    Serial.print(F("1,\r\n"));
  }
  else{ // unavailable mode
    Serial.print(F("0,\r\n"));
  }
}

/* Sets properly palette sampling.
 */
void setPaletteSampling(uint8_t temp){
  if(temp >= MIN_PALETTE_SAMPLING && temp <= MAX_PALETTE_SAMPLING){
    paletteSampling = temp;
    EEPROM.update(EEPROM_PALETTE_SAMPLING, paletteSampling);
    Serial.print(F("1,\r\n"));
  }
  else{
    Serial.print(F("0,\r\n"));
  }
}

/* Sets properly animation speed.
 */
void setAnimationSpeed(uint8_t temp){
  if(temp >= MIN_ANIMATION_SPEED && temp <= MAX_ANIMATION_SPEED){
    animationSpeed = temp;
    EEPROM.update(EEPROM_ANIMATION_SPEED, animationSpeed);
    Serial.print(F("1,\r\n"));
  }
  else{
    Serial.print(F("0,\r\n"));
  }
}

/* Sets properly palette animation direction.
 */
void setPaletteAnimationDirection(uint8_t temp){
  if(temp == 0 || temp == 1){
    paletteAnimationDirection = temp;
    EEPROM.update(EEPROM_PALETTE_ANIMATION_DIRECTION, paletteAnimationDirection);  
    Serial.print(F("1,\r\n"));
  }
  else{
    Serial.print(F("0,\r\n"));  
  }
}

/* Sets properly distribution to groups of LEDs.
 */
void setDistribution(uint8_t d1_t, uint8_t d2_t, uint8_t d3_t){   
  if((d1_t == 0 || d1_t == 1) && (d2_t == 0 || d2_t == 1) && (d3_t == 0 || d3_t == 1)){
    d1 = d1_t;
    EEPROM.update(EEPROM_D1, d1);
    d2 = d2_t;
    EEPROM.update(EEPROM_D2, d2);
    d3 = d3_t;
    EEPROM.update(EEPROM_D3, d3);
    Serial.print(F("1,\r\n"));
  }
  else{
    Serial.print(F("0,\r\n")); 
  }
}

/* Sets properly brightness.
 */
void setBrightness(int temp){
  if(temp >= 0 && temp <= 255){
    brightness = temp;
    EEPROM.update(EEPROM_BRIGHTNESS, brightness);
    FastLED.setBrightness(brightness); 
    FastLED.show(); 
    Serial.print(F("1,\r\n"));
  } 
  else{
    Serial.print(F("0,\r\n"));
  }
}

/* Sets properly FPS.
 */
void setFPS(int temp){
  if(temp >= MIN_FPS && temp <= MAX_FPS){
    FPS = temp;
    EEPROM.update(EEPROM_FPS, FPS);
    Serial.print(F("1,\r\n"));
  } 
  else{
    Serial.print(F("0,\r\n"));
  }
}

/* Check if there are any incomming data on serial, if yes save.
 * If '\n' was received, execute the stored command.
 */
void checkSerial(){
  if(Serial.available() > 0){ 
    serialReader._char = Serial.read();
    
    if(serialReader._char == '\n'){
      executeCommand(serialReader.string);
      serialReader.string[0] = '\0'; 
    }
    else{
      serialReader.string[strlen(serialReader.string) + 1] = '\0';
      serialReader.string[strlen(serialReader.string)] = serialReader._char;
    }
  }
}

/* Fill LEDs from palette, based on given starting index position on the palette. 
 * Fill LEDs with black, if distribution is disabled for particular segment/group.
 * While filling with black, it will not increase index of color palette, result is
 * continuous color palette across the enabled segments/group.
 */
void fillFromPalette(uint8_t colorIndex){
  uint8_t i;

  // table
  if(d1 == 1){
    for(i = TABLE_BEGIN_LED; i <= TABLE_END_LED; i++){
      leds[i] = ColorFromPalette(palette, colorIndex, brightness, BLENDING_TYPE);
      colorIndex += paletteSampling;
    }  
  }
  else{
    for(i = TABLE_BEGIN_LED; i <= TABLE_END_LED; i++){
      leds[i] = CRGB(0, 0, 0);
    }
  }

  // bed
  if(d2 == 1){
    for(i = BED_BEGIN_LED; i <= BED_END_LED; i++){
      leds[i] = ColorFromPalette(palette, colorIndex, brightness, BLENDING_TYPE);
      colorIndex += paletteSampling;
    }  
  }
  else{
    for(i = BED_BEGIN_LED; i <= BED_END_LED; i++){
      leds[i] = CRGB(0, 0, 0);
    }
  }

  // shelf
  if(d3 == 1){
    for(i = SHELF_BEGIN_LED; i <= SHELF_END_LED; i++){
      leds[i] = ColorFromPalette(palette, colorIndex, brightness, BLENDING_TYPE);
      colorIndex += paletteSampling;
    }  
  }
  else{
    for(i = SHELF_BEGIN_LED; i <= SHELF_END_LED; i++){
      leds[i] = CRGB(0, 0, 0);
    }
  }
}

/* Fill LEDs with given color, each for one segment/group.
 * If group is disabled, simply put black color.
 */
void setStaticColors(CRGB c1, CRGB c2, CRGB c3){
  uint8_t i;

  // table
  if(d1 == 1){
    for(i = TABLE_BEGIN_LED; i <= TABLE_END_LED; i++){
      leds[i] = c1;  
    }  
  }
  else{
    for(i = TABLE_BEGIN_LED; i <= TABLE_END_LED; i++){
      leds[i] = CRGB(0, 0, 0);  
    }  
  }

  // bed
  if(d2 == 1){
    for(i = BED_BEGIN_LED; i <= BED_END_LED; i++){
      leds[i] = c2;
    } 
  }
  else{
    for(i = BED_BEGIN_LED; i <= BED_END_LED; i++){
      leds[i] = CRGB(0, 0, 0);
    } 
  }

  // shelf
  if(d3 == 1){
    for(i = SHELF_BEGIN_LED; i <= SHELF_END_LED; i++){
      leds[i] = c3;
    }
  }
  else{
    for(i = SHELF_BEGIN_LED; i <= SHELF_END_LED; i++){
      leds[i] = CRGB(0, 0, 0);
    }  
  } 
}

/* Used for mode 2, automatically changes color of LEDs, based on given value.
 * Then changing brightness will take care of "pulsing effect".
 */
void colorAutoChange(uint8_t counter){
  if(counter == 0){
    setStaticColors(CRGB(127, 127, 0),
                    CRGB(127, 127, 0),
                    CRGB(127, 127, 0));
  }
  else if(counter == 1){
    setStaticColors(CRGB(0, 255, 0),
                    CRGB(0, 255, 0),
                    CRGB(0, 255, 0));
  }
  else if(counter == 2){
    setStaticColors(CRGB(0, 127, 127),
                    CRGB(0, 127, 127),
                    CRGB(0, 127, 127));
  }
  else if(counter == 3){
    setStaticColors(CRGB(0, 0, 127),
                    CRGB(0, 0, 127),
                    CRGB(0, 0, 127));
  }
  else if(counter == 4){
    setStaticColors(CRGB(127, 0, 127),
                    CRGB(127, 0, 127),
                    CRGB(127, 0, 127));
  }
  else if(counter == 5){
    setStaticColors(CRGB(200, 0, 0),
                    CRGB(200, 0, 0),
                    CRGB(200, 0, 0));
  }
}
